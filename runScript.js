const { exec } = require('child_process');

function runScript (script, isPipeStd = true) {
  return new Promise((resolve, rejects) => {
    const scriptStream = exec(script, (error) => {
      if (error)
        rejects(error)
      resolve()
    })
    if (isPipeStd) {
      scriptStream.stdout.pipe(process.stdout)
      scriptStream.stderr.pipe(process.stderr)
    }
  })
}
module.exports = runScript