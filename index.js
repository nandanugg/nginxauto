require('dotenv').config()
const { Select } = require('enquirer');
const removeHandler = require('./handler/removeHandler')
const createHandler = require('./handler/createHandler')

function quitGracefully (error) {
  process.exit(0)
}

async function main () {
  const prompt = new Select({
    name: 'navigate',
    message: 'What do you want?',
    choices: [
      'Add new config',
      'Remove existing config',
    ]
  })
  const result = await prompt.run()
    .catch(quitGracefully)
  if (result.startsWith('Add'))
    createHandler()
  if (result.startsWith('Remove'))
    removeHandler()
}
main()