const { prompt } = require('enquirer');
const fs = require('fs');
const { Transform } = require('stream');
const runScript = require('../runScript')

function quitGracefully (error) {
  if (error) {
    console.log(error);
  }
  process.exit(0)
}

async function newConfig (url, port) {
  const templateConfigFileStream = fs.createReadStream(process.env.NGINX_PATH + 'template.conf')
  const transformStream = new Transform({
    transform: function (chunk, encoding, cb) {
      this.push(chunk.toString()
        .replace(/\$DOMAIN_NAME/g, url)
        .replace(/\$PORT_NUMBER/g, port))
      cb()
    },
  })
  const writeStream = fs.createWriteStream(`${process.env.NGINX_CONFIG_PATH + url}:${port}.conf`)
  templateConfigFileStream.pipe(transformStream).pipe(writeStream)
}

async function createHandler () {
  const question = [
    {
      type: 'input',
      name: 'domain',
      message: 'What is your domain name?'
    },
    {
      type: 'input',
      name: 'port',
      message: 'In what port is your app running?'
    },
    {
      type: 'confirm',
      name: 'isGetCert',
      message: 'Do you want also get cert?'
    },
  ];
  const { domain, port, isGetCert } = await prompt(question)
    .catch(() => quitGracefully());
  if (isGetCert) {
    const { email } = await prompt({
      type: 'input',
      name: 'email',
      message: 'What is your email?'
    })
    await runScript(`docker exec certbot certbot certonly --webroot -w /var/www/_letsencrypt -n --agree-tos --force-renewal -d ${domain} --email ${email}`)
      .catch(quitGracefully)
  }
  await newConfig(domain, port)
  await runScript(`docker restart nginx`)
    .catch(quitGracefully)
  console.log('config created');
}

module.exports = createHandler