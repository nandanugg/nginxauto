const { MultiSelect, Confirm } = require('enquirer');
const fs = require('fs');
const { promisify } = require('util');
const readdir = promisify(fs.readdir)
const runScript = require('../runScript')

async function deleteConfig (fileNames) {
  const promises = fileNames.map(fileName => {
    return new Promise(resolve => {
      fs.unlink(process.env.NGINX_CONFIG_PATH + fileName, resolve)
    })
  })
  return Promise.all(promises)
}

function quitGracefully (error) {
  if (error) {
    console.log(error);
  }
  process.exit(0)
}

async function removeConfigHandler () {
  let configList = await readdir(process.env.NGINX_CONFIG_PATH)
  configList = configList
    .filter((configFile) => !configFile.startsWith('http.conf'))
    .map(configFile => {
      return {
        fileName: configFile,
        domainName: configFile.split(':')[0],
        humanName: configFile
          .replace('.conf', '')
          .replace(':', ' => ')
      }
    })

  if (!configList.length) {
    console.log("No config detected");
    process.exit(0)
  } else {
    const prompt = new MultiSelect({
      name: 'pick',
      message: 'Choose which config to delete (press space to select)',
      choices: configList
        .map(({ humanName }) => humanName)
    })
    let results = await prompt
      .run()
      .catch(() => quitGracefully());

    const deletedFiles = await configList
      .filter(({ humanName }) => results.includes(humanName))
    if (!deletedFiles.length)
      quitGracefully()

    await deleteConfig(deletedFiles.map(({ fileName }) => fileName))

    const removeCertPrompt = new Confirm({
      name: 'question',
      message: 'Do you want to remove the cert?'
    })
    const isRemoveCert = await removeCertPrompt
      .run()
      .catch(() => quitGracefully());
    if (isRemoveCert) {
      const deletedDomains = deletedFiles.map(({ domainName }) => domainName)
      const deletedPromises = deletedDomains.map(domain =>
        runScript(`docker exec certbot certbot delete --cert-name ${domain} && docker restart nginx`))

      deletedPromises.reduce((promiseChain, currentTask) => {
        return promiseChain.then(chainResults =>
          currentTask.then(currentResult =>
            [...chainResults, currentResult]
          )
        );
      }, Promise.resolve([])).then(arrayOfResults => {
        console.log('done');
      });
    }
  }

}

module.exports = removeConfigHandler